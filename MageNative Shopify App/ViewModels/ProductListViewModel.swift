/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class ProductListViewModel: ViewModel {
   
    
    typealias ModelType = Storefront.ProductEdge
    let productModel:Storefront.Product.ViewModelType?
    let cursor:   String?
    let model : ModelType? 
    let id:       String
    let title:    String
    let summary:  String
    let price:    String
    let compareAtPrice:String
    let images:   PageableArray<ImageViewModel>
    let variants: PageableArray<VariantViewModel>
    let onlineStoreUrl:URL?
    
    
    // ----------------------------------
    //  MARK: - Init -
    //
    required init(from model: ModelType) {
        self.model    = model
        self.cursor   = model.cursor
        self.productModel = nil
        let variants = model.node.variants.edges.viewModels.sorted {
            $0.price < $1.price
        }
        let lowestPrice = variants.first?.price
        self.id       = model.node.id.rawValue
        self.title    = model.node.title
        self.summary  = model.node.descriptionHtml
        self.price    = lowestPrice == nil ? "false" : Currency.stringFrom(lowestPrice!)
        self.onlineStoreUrl = model.node.onlineStoreUrl
        
        self.images   = PageableArray(
            with:     model.node.images.edges,
            pageInfo: model.node.images.pageInfo
        )
        let compareAtprice = variants.first?.compareAtPrice
        self.compareAtPrice = compareAtprice == nil ? "false" : Currency.stringFrom(compareAtprice!)

        self.variants = PageableArray(
             with:     model.node.variants.edges,
            pageInfo: model.node.variants.pageInfo
        )
   
    }
    
    // ----------------------------------
    //  MARK: - Init -
    //
    init(from model:ProductViewModel) {
        self.cursor = "Null"
        self.productModel = model
        self.model = nil
        self.id       = model.id
        self.title    = model.title
        self.summary  = model.summary
        self.price    =  model.price
        self.onlineStoreUrl = model.onlineStoreUrl
        
        self.images   = model.images
        self.compareAtPrice = model.compareAtPrice
        
        self.variants = model.variants
    }
    
    
}

extension Storefront.ProductEdge: ViewModeling {
    typealias ViewModelType = ProductListViewModel
}
