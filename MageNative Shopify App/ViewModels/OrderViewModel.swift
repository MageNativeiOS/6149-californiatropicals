/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import Foundation
class OrderViewModel:ViewModel{
    typealias ModelType = Storefront.OrderEdge
    let model: ModelType?
    let processedAt:String
    let customerUrl:URL?
    let totalPrice:String
    let orderNumber:String
    let statusUrl:URL?
    let shippingAddress:AddressViewModel?
    let lineItems:PageableArray<OrderLineItemViewModel>
    required init(from model: ModelType) {
       self.model = model
       self.customerUrl =  model.node.customerUrl
       self.processedAt =  model.node.processedAt.formatDate(with: "MMM dd yyyy")
       self.orderNumber = String(model.node.orderNumber)
       self.totalPrice  = Currency.stringFrom(model.node.totalPrice)
       self.shippingAddress =  model.node.shippingAddress?.viewModel
       self.statusUrl = model.node.statusUrl
        
       self.lineItems =  PageableArray (
        with: model.node.lineItems.edges,
        pageInfo: model.node.lineItems.pageInfo
        )
        
    }
}
extension Storefront.OrderEdge:ViewModeling{
    typealias ViewModelType = OrderViewModel
}
