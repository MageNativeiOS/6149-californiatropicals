/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */
import Foundation

class ProductViewModel: ViewModel {
    typealias ModelType = Storefront.Product
    
    let model : ModelType?
    let id:       String
    let title:    String
    let summary:  String
    let price:    String
    let compareAtPrice:String
    let images:   PageableArray<ImageViewModel>
    let variants: PageableArray<VariantViewModel>
    let onlineStoreUrl:URL?
    
    // ----------------------------------
    //  MARK: - Init -
    //
    required init(from model: ModelType) {
        self.model    = model
        
        
        let variants = model.variants.edges.viewModels.sorted {
            $0.price < $1.price
        }
        let lowestPrice = variants.first?.price
        self.id       = model.id.rawValue
        self.title    = model.title
        self.summary  = model.descriptionHtml
        self.price    = lowestPrice == nil ? "false" : Currency.stringFrom(lowestPrice!)
        self.onlineStoreUrl = model.onlineStoreUrl
        
        self.images   = PageableArray(
            with:     model.images.edges,
            pageInfo: model.images.pageInfo
        )
        let specialPrice = variants.first?.compareAtPrice
        self.compareAtPrice = specialPrice == nil ? "false" : ( lowestPrice! < specialPrice! ? Currency.stringFrom(specialPrice!) : "false" )
        
        self.variants = PageableArray(
            with:     model.variants.edges,
            pageInfo: model.variants.pageInfo
        )
        
    }
}

extension Storefront.Product: ViewModeling {
    typealias ViewModelType = ProductViewModel
}
