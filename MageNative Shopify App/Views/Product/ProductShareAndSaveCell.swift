/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

protocol shareNsave {
    func shareProduct(_cell:ProductShareAndSaveCell,sender:Any)
    func saveProduct(_cell:ProductShareAndSaveCell,sender:Any)
}

class ProductShareAndSaveCell: UITableViewCell {

    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var SaveButton: UIButton!
    var delegate:shareNsave?
    override func awakeFromNib() {
        super.awakeFromNib()
        shareButton.addTarget(self, action: #selector(self.shareProduct(sender:)), for: .touchUpInside)
         SaveButton.addTarget(self, action: #selector(self.saveProduct(sender:)), for: .touchUpInside)
        
        SaveButton.ButtonTextDown(spacing: 5)
        shareButton.ButtonTextDown(spacing: 5)
    }

    func setUPWishImage(model:ProductViewModel){
        if WishlistManager.shared.isProductinWishlist(product: model){
            SaveButton.setImage(#imageLiteral(resourceName: "wishfilled"), for: .normal)
        }else {
            SaveButton.setImage(#imageLiteral(resourceName: "wishempty"), for: .normal)
        }
    }
    
    @objc func shareProduct(sender:UIButton){
        self.delegate?.shareProduct(_cell: self, sender: sender)
    }
    
    @objc func saveProduct(sender:UIButton){
        self.delegate?.saveProduct(_cell: self,sender: sender)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
