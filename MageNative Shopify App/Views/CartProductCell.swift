/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

protocol cartQuantityUpdate:class {
    func updateCartQuantity(sender:CartProductCell,quantity:Int,model:CartViewModel?)
}

class CartProductCell: UITableViewCell {
    
    @IBOutlet weak var incrementQty: UIButton!
    @IBOutlet weak var decrementQty: UIButton!
    @IBOutlet weak var productQty: UITextField!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var variantTitle: UILabel!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    var delegate:cartQuantityUpdate?
    var model:CartViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        productQty.textAlignment = .center;
        incrementQty.addTarget(self, action: #selector(self.incrementQty(sender:)), for: .touchUpInside)
        decrementQty.addTarget(self, action: #selector(self.decrementQty(sender:)), for: .touchUpInside)
    }
    
    
    func configure(from model:CartViewModel){
        self.model = model
        self.productImage.setImageFrom(model.imageURL)
        self.productTitle.text = model.title
        if model.subtitle == "Default Title"{
            self.variantTitle.text =  ""
        }else{
            self.variantTitle.text =  model.subtitle
        }
        self.productPrice.text = model.price
        self.productQty.text =  String(model.quantity)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    @objc func incrementQty(sender:UIButton){
        guard let qtyText = self.productQty.text else {return}
        guard  var qty = Int(qtyText) else {return}
        qty += 1
        delegate?.updateCartQuantity(sender: self, quantity:qty, model: self.model)
    }
    
    @objc func decrementQty(sender:UIButton){
        guard let qtyText = self.productQty.text else {return}
        guard  var qty = Int(qtyText) else {return}
        qty -= 1
        //if qty <= 0  {return}
        delegate?.updateCartQuantity(sender: self, quantity:qty, model: self.model)
    }
    
}
