/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class MyOrdersCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var calenderButton: UIButton!
    @IBOutlet weak var statusButton: UIButton!
   
    @IBOutlet weak var totalItems: UILabel!
    @IBOutlet weak var shipTo: UILabel!
    @IBOutlet weak var orderId: UILabel!
    
    @IBOutlet weak var cost: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       // calenderButton.setImage(UIImage(named:"calendar"), for: UIControlState.normal)
        //calenderButton.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
    }
    
    func configureFrom(_ model:OrderViewModel) {
        orderId.text = model.orderNumber
        cost.text = model.totalPrice
        shipTo.text = model.shippingAddress?.name
        totalItems.text = String(model.lineItems.items.count)
        calenderButton.setTitle(model.processedAt.description, for: .normal)
        statusButton.setTitle(model.model!.node.fulfillmentStatus.rawValue, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
