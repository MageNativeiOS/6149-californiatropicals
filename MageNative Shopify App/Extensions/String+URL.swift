/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import Foundation

extension String {
    func getURL()-> URL?{
        do {
            return try self.asURL()
        }catch {
            return nil
        }
    }
}

extension String {
    var localized: String {
        let lang = "en"
        //let path = Bundle.main.path(forResource: lang, ofType: "lproj");
        //let bundle = Bundle(path: path!);
        return NSLocalizedString(self, comment: "")//NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "");
    }
    public func isValidEmail() -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}";
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx);
        return emailTest.evaluate(with: self);
        
    }
    public func isValidName() -> Bool
    {
        let RegEx = "[a-zA-Z ]*$";
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx);
        return Test.evaluate(with: self);
        
    }
}
