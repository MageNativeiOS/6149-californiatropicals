/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import Foundation

final class ClientQuery {

    static let maxImageDimension = Int32(UIScreen.main.bounds.width)
    // ----------------------------------
    //  MARK: - Shop -
    //
    static func queryForShopName() -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .shop { $0
                .name()
            }
        }
    }
    
    // ----------------------------------
    //  MARK: - LOGIN Customer Token -
    //
   static func queryForCustomerToken( email:String,password:String)->Storefront.MutationQuery{
        let accessTokenInput = Storefront.CustomerAccessTokenCreateInput(email: email, password: password)
        return Storefront.buildMutation{$0
            .customerAccessTokenCreate(input: accessTokenInput, {$0
                .customerAccessToken{$0
                    .accessToken()
                    .expiresAt()
                }
                .userErrors{$0
                    .field()
                    .message()                    
                }
            })
            
        }
    }
    
    // ----------------------------------
    //  MARK: - Update Customer Token -
    //
    
   static func queryForCustomerTokenUpdate(accessToken:String)->Storefront.MutationQuery{
        return Storefront.buildMutation{$0
            .customerAccessTokenRenew(customerAccessToken: accessToken, {$0
                .customerAccessToken{$0
                    .accessToken()
                    .expiresAt()
                }
                
            })
            
        }
    }
  
    
    
    // ----------------------------------
    //  MARK: - SIGNUP  -
    //
    static func queryForSignUp(email:String,password:String,firstName:String,lastName:String,acceptsMarketing:Bool)->Storefront.MutationQuery{
        
        let customerInput = Storefront.CustomerCreateInput.create(email: email, password: password, firstName: Input(orNull: firstName), lastName: Input(orNull: lastName), acceptsMarketing: Input(orNull:acceptsMarketing))
    
        return Storefront.buildMutation{ $0
            .customerCreate(input: customerInput, {$0
                .customer{$0
                    .createdAt()
                    .firstName()
                    .lastName()
                    .id()
                    .acceptsMarketing()
                    .email()
                    .phone()
                    .displayName()
                }
                .userErrors{$0
                    .field()
                    .message()
                    
                }
                
            })
            
        }
    }
    
    
    // ----------------------------------
    //  MARK: - Storefront -
    //
    static func queryForCollections(limit: Int, after cursor: String? = nil, productLimit: Int = 25, productCursor: String? = nil,maxHeight:Int32,maxWidth:Int32) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .shop { $0
                .collections(first: Int32(limit), after: cursor) { $0
                    .pageInfo { $0
                        .hasNextPage()
                    }
                    .edges { $0
                        .cursor()
                        .node { $0
                            .id()
                            .title()
                            
                            .descriptionHtml()
                            .image(maxWidth: maxWidth, maxHeight: maxHeight) { $0
                                .transformedSrc()
                            }
                            

                        }
                    }
                    
                }
                
            }
            
        }
    }
    
    static func queryForProducts(in collection: CollectionViewModel? =  nil,coll:collection? = nil,with sortKey:Storefront.ProductCollectionSortKeys? = nil,reverse:Bool?=nil, limit: Int, after cursor: String? = nil) -> Storefront.QueryRootQuery {
        var id : GraphQL.ID!
        if  let collection = collection?.model?.node.id {
            id = collection
        }else {
            if let collId = coll?.id {
                let str1 = ("gid://shopify/Collection/"+collId).data(using: String.Encoding.utf8)
                let base64 = str1!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                print(base64)
                id = GraphQL.ID(rawValue: base64)
            }
            
        }
      
        return Storefront.buildQuery { $0
            .node(id: id) { $0
                .onCollection { $0
                    .products(first: Int32(limit), after: cursor,reverse: reverse,sortKey:sortKey
                    ) { $0
                        .fragmentForStandardProduct()
                    }
                    .image(maxWidth: ClientQuery.maxImageDimension, maxHeight:  ClientQuery.maxImageDimension){$0
                        .transformedSrc()
                    }
                    
                 
                }
                
            }
        }
    }
    
    
    static func queryForSingleProduct(of Id: GraphQL.ID? = nil) -> Storefront.QueryRootQuery {
        
        return Storefront.buildQuery { $0
            .node(id: Id!){ $0
                    .onProduct{ $0
                        .id()
                        .title()
                        .descriptionHtml()
                        .variants(first: 250) { $0
                            .fragmentForStandardVariant()
                        }
                    .availableForSale()
                    .handle()
                        .images(first: 250, maxWidth: ClientQuery.maxImageDimension, maxHeight: ClientQuery.maxImageDimension) { $0
                            .fragmentForStandardProductImage()
                        }
                        .onlineStoreUrl()
                    }
       
                }
                
            }
        
    }
    
    
    //-----------------------------------
    // MARK: - Search Product Query -
    //
    static func queryForSearchProducts(for query: String, limit: Int, after cursor: String? = nil,with sortKey:Storefront.ProductSortKeys? = nil,reverse:Bool?=nil) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            
            .shop{$0
                .products(first: Int32(limit), after: cursor, reverse: reverse, sortKey:sortKey, query: query){$0
                    .fragmentForStandardProduct()
                    
                }
            }
        }
    }
    
    
    //-----------------------------------
    // MARK: - Order -
    //
    static func queryForOrders(of customerToken: String, limit: Int, after cursor: String? = nil)-> Storefront.QueryRootQuery {
       return Storefront.buildQuery {$0
            .customer(customerAccessToken: customerToken, {$0
                .orders(first: Int32(limit), after: cursor,reverse: true,sortKey: Storefront.OrderSortKeys.processedAt){$0
                        .fragmentForStandardOrder()
                }
            })
        }
        
    }

    // ----------------------------------
    //  MARK: - Checkout -
    //
    static func mutationForCreateCheckout(with cartItems: [CartProduct]) -> Storefront.MutationQuery {
        let lineItems = cartItems.map { item in
            Storefront.CheckoutLineItemInput.create(quantity: Int32(item.qty), variantId: GraphQL.ID(rawValue: item.variant.id))
        }
        
        let checkoutInput = Storefront.CheckoutCreateInput.create(
            lineItems: .value(lineItems),
            allowPartialAddresses: .value(true)
        )
        
        return Storefront.buildMutation { $0
            .checkoutCreate(input: checkoutInput) { $0
                .checkout { $0
                    .fragmentForCheckout()
                }
                .userErrors{$0
                    .field()
                    .message()
                }
                
            }
        }
    }
    
    // ----------------------------------
    //  MARK: - ForgotPassword -
    //
    static func mutationForForgetPassword(with email: String)-> Storefront.MutationQuery
    {
        return Storefront.buildMutation{ $0
            .customerRecover(email: email){ $0
                .userErrors{ $0
                    .message()
                    .field()
                }
            }
        }
    }
    
    // -----------------------------------
    // MARK : - RenewToken -
    //
    
    static func mutationForRenewToken(accessToken:String)->Storefront.MutationQuery {
        return Storefront.buildMutation{$0
            .customerAccessTokenRenew(customerAccessToken: accessToken){$0
                .customerAccessToken{$0
                    .accessToken()
                    .expiresAt()
                }
                .userErrors{$0
                    .field()
                    .message()
                }
            }
        }
    }
    
    
    //Apply discountCode Query
    static func mutationForApplyCopounCode(_ discountCode: String, to checkoutID: String)->Storefront.MutationQuery{
        let id = GraphQL.ID(rawValue: checkoutID)
        return Storefront.buildMutation { $0
            .checkoutDiscountCodeApply(discountCode: discountCode, checkoutId: id) { $0
                .userErrors { $0
                    .field()
                    .message()
                }
                .checkout { $0
                    .fragmentForCheckout()
                }
            }
        }
       
    }
    
    //---------------------------------
    // Mark : - Fetch Customer Details
    static func queryForCustomer(accessToken:String)->Storefront.QueryRootQuery{
        return Storefront.buildQuery{$0
            .customer(customerAccessToken: accessToken){$0
                .firstName()
                .lastName()
                .email()
                .displayName()
                .id()
                .createdAt()
                
            }
            
        }
    }
    
    
    //---------------------------------
    // Mark : - Fetch Customer Addresses Details
    static func queryForAddress(accessToken:String,limit: Int, after cursor: String? = nil)->Storefront.QueryRootQuery{
        return Storefront.buildQuery{$0
            .customer(customerAccessToken: accessToken){$0
                .addresses(first: Int32(limit), after: cursor){$0
                    .fragmentForStandardAddress()
                }
                
            }
        }
    }
    
    //---------------------------------
    // Mark : - Fetch Shop Details
    //
    static func queryForShop()->Storefront.QueryRootQuery{
        return Storefront.buildQuery { $0
            .shop { $0
                .name()
                .privacyPolicy{$0
                    .title()
                    .url()
                }
                .termsOfService{$0
                    .title()
                    .url()
                }
                .refundPolicy{ $0
                    .url()
                    .title()
                }
                .paymentSettings{
                    $0
                        .currencyCode()
                }
                
            }
        }
    }
    
    
    //---------------------------------
    // Mark : - Fetch Shop Details
    //
    static func queryForAllShopProducts(with sortKey:Storefront.ProductSortKeys? = nil,reverse:Bool?=nil, limit: Int, after cursor: String? = nil)->Storefront.QueryRootQuery{
        return Storefront.buildQuery { $0
            .shop { $0
                .products(first: Int32(limit), after: cursor,reverse: reverse,sortKey:sortKey
                ){
                    $0.fragmentForStandardProduct()
                }
                
            }
        }
    }
    
    
    //---------------------------------
    // Mark : - Customer Add Address
    //
    static func customerAddAddressQuery(accessToken:String,address:[String:String])->Storefront.MutationQuery{
        
        let addressInput = Storefront.MailingAddressInput.create(address1: Input(orNull:address["address1"]), address2: Input(orNull:address["address2"]), city: Input(orNull:address["city"]), country: Input(orNull:address["country"]), firstName: Input(orNull:address["firstName"]), lastName: Input(orNull:address["lastName"]), phone: Input(orNull:address["phone"]), province: Input(orNull:address["province"]), zip: Input(orNull:address["zip"]))
        
        return Storefront.buildMutation{$0
            .customerAddressCreate(customerAccessToken: accessToken, address: addressInput){$0
                .userErrors{$0
                    .field()
                    .message()
                }
                
                .customerAddress{$0
                    .fragmentForStandardMailAddress()
                }
            }
            
        }
    }
    
        //---------------------------------
     // Mark : - Customer Update Address
     //
     static func customerUpdateAddressQuery(accessToken:String,addressId:GraphQL.ID,address:[String:String])->Storefront.MutationQuery{
         
         let addressInput = Storefront.MailingAddressInput.create(address1: Input(orNull:address["address1"]), address2: Input(orNull:address["address2"]), city: Input(orNull:address["city"]), country: Input(orNull:address["country"]), firstName: Input(orNull:address["firstName"]), lastName: Input(orNull:address["lastName"]), phone: Input(orNull:address["phone"]), province: Input(orNull:address["province"]), zip: Input(orNull:address["zip"]))
         
         return Storefront.buildMutation{$0
             .customerAddressUpdate(customerAccessToken: accessToken, id: addressId, address: addressInput){$0
                 .customerAddress{$0
                     .fragmentForStandardMailAddress()
                 }
                 .userErrors{$0
                     .field()
                     .message()
                 }
             }
         }
     }
     
     
     //---------------------------------
     //MARK : - Customer Delete Address
     //
    static func customerDeleteAddress(addressId:GraphQL.ID?,with token:String)->Storefront.MutationQuery{
         return Storefront.buildMutation({$0
             .customerAddressDelete(id: addressId!, customerAccessToken: token){$0
                 .userErrors{$0
                     .field()
                     .message()
                 }
                 .deletedCustomerAddressId()
                 
             }
             
         })
     }
    static func customerUpdateDetails(accessToken:String,email:String,password:String,firstName:String,lastName:String)->Storefront.MutationQuery{
        
        let customer = Storefront.CustomerUpdateInput.create(firstName: Input(orNull: firstName), lastName: Input(orNull: lastName), email: Input(orNull:email))
        if password != "" {
            customer.password = Input(orNull:password)
        }
        return Storefront.buildMutation{ $0
            .customerUpdate(customerAccessToken: accessToken, customer: customer){ $0
                .customer{ $0
                    .firstName()
                    .lastName()
                    .email()
                    .displayName()
                    .id()
                    .createdAt()
                }
                .userErrors{ $0
                    .message()
                    .field()
                    
                }
                
            }
        }
    }
    
    static func mutationForUpdateCheckout(_ id: String, updatingPartialShippingAddress address: PayPostalAddress) -> Storefront.MutationQuery {
        
        let checkoutID   = GraphQL.ID(rawValue: id)
        let addressInput = Storefront.MailingAddressInput.create(
            city:     address.city.orNull,
            country:  address.country.orNull,
            province: address.province.orNull,
            zip:      address.zip.orNull
        )
        
        return Storefront.buildMutation { $0
            .checkoutShippingAddressUpdate(shippingAddress: addressInput, checkoutId: checkoutID) { $0
                .userErrors { $0
                    .field()
                    .message()
                }
                .checkout { $0
                    .fragmentForCheckout()
                }
            }
        }
    }
    
    static func mutationForUpdateCheckout(_ id: String, updatingCompleteShippingAddress address: PayAddress) -> Storefront.MutationQuery {
        
        let checkoutID   = GraphQL.ID(rawValue: id)
        let addressInput = Storefront.MailingAddressInput.create(
            address1:  address.addressLine1.orNull,
            address2:  address.addressLine2.orNull,
            city:      address.city.orNull,
            country:   address.country.orNull,
            firstName: address.firstName.orNull,
            lastName:  address.lastName.orNull,
            phone:     address.phone.orNull,
            province:  address.province.orNull,
            zip:       address.zip.orNull
        )
        
        return Storefront.buildMutation { $0
            .checkoutShippingAddressUpdate(shippingAddress: addressInput, checkoutId: checkoutID) { $0
                .userErrors { $0
                    .field()
                    .message()
                }
                .checkout { $0
                    .fragmentForCheckout()
                }
            }
        }
    }
    
    static func mutationForUpdateCheckout(_ id: String, updatingShippingRate shippingRate: PayShippingRate) -> Storefront.MutationQuery {
        
        return Storefront.buildMutation { $0
            .checkoutShippingLineUpdate(checkoutId: GraphQL.ID(rawValue: id), shippingRateHandle: shippingRate.handle) { $0
                .userErrors { $0
                    .field()
                    .message()
                }
                .checkout { $0
                    .fragmentForCheckout()
                }
            }
        }
    }
    
    static func mutationForUpdateCheckout(_ id: String, updatingEmail email: String) -> Storefront.MutationQuery {
        
        return Storefront.buildMutation { $0
            .checkoutEmailUpdate(checkoutId: GraphQL.ID(rawValue: id), email: email) { $0
                .userErrors { $0
                    .field()
                    .message()
                }
                .checkout { $0
                    .fragmentForCheckout()
                }
            }
        }
    }
    
    static func mutationForCompleteCheckoutUsingApplePay(_ checkout: PayCheckout, billingAddress: PayAddress, token: String, idempotencyToken: String) -> Storefront.MutationQuery {
        
        let mailingAddress = Storefront.MailingAddressInput.create(
            address1:  billingAddress.addressLine1.orNull,
            address2:  billingAddress.addressLine2.orNull,
            city:      billingAddress.city.orNull,
            country:   billingAddress.country.orNull,
            firstName: billingAddress.firstName.orNull,
            lastName:  billingAddress.lastName.orNull,
            province:  billingAddress.province.orNull,
            zip:       billingAddress.zip.orNull
        )
        
        let paymentInput = Storefront.TokenizedPaymentInput.create(
            amount:         checkout.paymentDue,
            idempotencyKey: idempotencyToken,
            billingAddress: mailingAddress,
            type:           CheckoutViewModel.PaymentType.applePay.rawValue,
            paymentData:    token
        )
        
        return Storefront.buildMutation { $0
            .checkoutCompleteWithTokenizedPayment(checkoutId: GraphQL.ID(rawValue: checkout.id), payment: paymentInput) { $0
                .userErrors { $0
                    .field()
                    .message()
                }
                .payment { $0
                    .fragmentForPayment()
                }
            }
        }
    }
    
    static func queryForPayment(_ id: String) -> Storefront.QueryRootQuery {
        return Storefront.buildQuery { $0
            .node(id: GraphQL.ID(rawValue: id)) { $0
                .onPayment { $0
                    .fragmentForPayment()
                }
            }
        }
    }
    
    static func queryShippingRatesForCheckout(_ id: String) -> Storefront.QueryRootQuery {
        
        return Storefront.buildQuery { $0
            .node(id: GraphQL.ID(rawValue: id)) { $0
                .onCheckout { $0
                    .fragmentForCheckout()
                    .availableShippingRates { $0
                        .ready()
                        .shippingRates { $0
                            .handle()
                            .price()
                            .title()
                        }
                    }
                }
            }
        }
    }
    
}
