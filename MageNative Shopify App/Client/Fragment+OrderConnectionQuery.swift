/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import Foundation
extension Storefront.OrderConnectionQuery {
    
    @discardableResult
    func fragmentForStandardOrder() -> Storefront.OrderConnectionQuery { return self
        .pageInfo { $0
            .hasNextPage()
        }
        .edges { $0
            .cursor()
            .node { $0
                .totalPrice()
                .id()
                .orderNumber()
                .totalPrice()
                .customerLocale()
                .customerUrl()
                .email()
                .statusUrl()
                .phone()
                .fulfillmentStatus()
                .subtotalPrice()
                .totalTax()
                .totalShippingPrice()
                .processedAt()
                .currencyCode()
                .shippingAddress{$0
                    .fragmentForStandardMailAddress()
                    
                }
                .lineItems(first: 250) { $0
                    .edges { $0
                        .cursor()
                        .node { $0
                            .variant { $0
                                .id()
                                .price()
                            }
                            .title()
                            .quantity()
                        }
                    }
                    .pageInfo{$0
                        .hasNextPage()
                        
                    }
                }
            }
        }
    }
}
