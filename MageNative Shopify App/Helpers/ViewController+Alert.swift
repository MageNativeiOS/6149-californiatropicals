/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import Foundation
extension UIViewController {
    func showErrorAlert(errors:[UserErrorViewModel]?){
        guard let errors = errors else {return}
        var errorMsg = ""
        errors.forEach{
            errorMsg += $0.errorMessage + "\n"
        }
        let alertViewController = UIAlertController(title: "Error".localized, message: errorMsg, preferredStyle: .alert)
        alertViewController.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    
    func showErrorAlert(title:String? = nil,error:String?){
        guard let error = error else {return}
        
        let alertViewController = UIAlertController(title: title, message: error, preferredStyle: .alert)
        alertViewController.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    func setupTabbarCount(){
        self.tabBarController?.tabBar.items?[2].badgeValue = CartManager.shared.cartCount.description
        self.tabBarController?.tabBar.items?[3].badgeValue = WishlistManager.shared.wishCount.description
    }
    
  
    
}
