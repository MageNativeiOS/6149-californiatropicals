/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import Foundation
import UserNotifications
class CartManager {
    static let shared = CartManager()
    private let backQueue    = DispatchQueue(label: "com.magenative.Queue")
    
    var cartProducts : [CartProduct] = []
    private var saveCart = false
    var cartSubtotal: Decimal {
        return self.cartProducts.reduce(0) {
            $0 + $1.variant.price * Decimal($1.qty)
        }
    }
    
    var cartCount: Int {
        return self.cartProducts.reduce(0) {
            $0 + $1.qty
        }
    }
    
    private var cartURL: URL = {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let documentsURL  = URL(fileURLWithPath: documentsPath)
        let cartURL       = documentsURL.appendingPathComponent("\(Client.shopUrl).json")
        
        print("Cart URL: \(cartURL)")
        
        return cartURL
    }()
    
    
    
    private init() {
        self.readCart { items in
            if let items = items {
                self.cartProducts = items
                if self.cartCount > 0 {
                 self.getAccessLocalNotification()
                }else {
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["magenative_cart_notification_center"])
                }
            }
        }
    }
    
    private func saveCartData() {
        let serializedItems = self.cartProducts.serialize()
        self.backQueue.async {
            do {
                let data = try JSONSerialization.data(withJSONObject: serializedItems, options: [])
                try data.write(to: self.cartURL, options: [.atomic])
                
                
            } catch let error {
                print("\(error)")
            }
            
            DispatchQueue.main.async {
                self.saveCart = false
            }
        }
    }
    
    private func readCart(completion: @escaping ([CartProduct]?) -> Void) {
        self.backQueue.async {
            do {
                let data            = try Data(contentsOf: self.cartURL)
                let serializedItems = try JSONSerialization.jsonObject(with: data, options: [])
                
                let cartItems = [CartProduct].deserialize(from: serializedItems as! [SerializedRepresentation])
                DispatchQueue.main.async {
                    completion(cartItems)
                }
                
            } catch let error {
                print("Failed to load cart from disk: \(error)")
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
    
    
    func updateCartQuantity(at index:Int ,with qty:Int){
     if qty == 0{
         self.deleteQty(at: index)
      }else{
          self.cartProducts[index].qty = qty
          self.saveCartData()
      }
      
    }
    
    
    func addToCart(_ product:CartProduct){
        if let index = self.cartProducts.index(of: product)  {
            self.cartProducts[index].qty += 1
        }else {
            self.cartProducts.append(product)
        }
        self.saveCartData()
        
    }
    
    
    func deleteQty(at index:Int){
        self.cartProducts.remove(at: index)
        self.saveCartData()
    }
    
    func deleteAll(){
        self.cartProducts.removeAll()
        self.saveCartData()
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["magenative_cart_notification_center"])
    }
    
    
    func showCartQuantityError(on view:UIViewController,error:[UserErrorViewModel]?){
        guard let error = error else {return}
        var msg = ""
        error.forEach{
            guard let errors =  $0.errorFields else {return}
            guard let index = Int(errors[2]) else {return}
            let title = CartManager.shared.cartProducts[index].productModel.title
            msg += title + "\n" + $0.errorMessage + "\n"
        }
        msg += "So we are updating quantity in cart.".localized
        let alertViewController = UIAlertController(title: "Quantity Error".localized, message: msg, preferredStyle: .alert)
        alertViewController.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
        view.present(alertViewController, animated: true, completion: nil)
    }
}
