/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */
import Foundation
class WishlistManager {
    static let shared = WishlistManager()
    var wishListProducts : [CartProduct] = []
    private let backQueue    = DispatchQueue(label: "com.magenative.Queue")
    private var saveWish = false
    private var wishURL: URL = {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let documentsURL  = URL(fileURLWithPath: documentsPath)
        let cartURL       = documentsURL.appendingPathComponent("\(Client.shopUrl)wish.json")
        
     
        
        return cartURL
    }()
    
    var wishCount: Int {
        return self.wishListProducts.reduce(0) {
            $0 + $1.qty
        }
    }
    
    
    private init() {
        self.readWishList { items in
            if let items = items {
                self.wishListProducts = items
            }
        }
    }
    
    private func saveWishData() {
        let serializedItems = self.wishListProducts.serialize()
        self.backQueue.async {
            do {
                let data = try JSONSerialization.data(withJSONObject: serializedItems, options: [])
                try data.write(to: self.wishURL, options: [.atomic])
                
                
            } catch let error {
                print("\(error)")
            }
            
            DispatchQueue.main.async {
                self.saveWish = false
            }
        }
    }
    
    
    private func readWishList(completion: @escaping ([CartProduct]?) -> Void) {
        self.backQueue.async {
            do {
                let data            = try Data(contentsOf: self.wishURL)
                let serializedItems = try JSONSerialization.jsonObject(with: data, options: [])
                
                let wishListItems = [CartProduct].deserialize(from: serializedItems as! [SerializedRepresentation])
                DispatchQueue.main.async {
                    completion(wishListItems)
                }
                
            } catch let error {
                print("Failed to load cart from disk: \(error)")
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
    
    func removeFromWishList(_ product:CartProduct){
        guard let index = self.wishListProducts.index(of: product) else {return}
        self.wishListProducts.remove(at: index)
         self.saveWishData()
    }
    
    
    func addToWishList(_ product:CartProduct){
        if let index = self.wishListProducts.index(of: product)  {
            self.wishListProducts[index].qty += 1
        }else {
            self.wishListProducts.append(product)
        }
        self.saveWishData()
    }
    
    func isProductinWishlist(product:ProductViewModel) -> Bool{
        return WishlistManager.shared.wishListProducts.contains(where: {$0.productModel.id == product.id})
    }
}
