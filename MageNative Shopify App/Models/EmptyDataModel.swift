/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import Foundation
class EmptyData {
    static let homeTitle = "Unable To Load Data".localized
    static let homeDesciption = "Please check your network connection and try again.".localized
    
    static let cartEmptyTitle = "No Products Found In Your Cart!".localized
     static let cartDescription = "Shop your favourite brands,products.".localized
    
    
    static let listEmptyTitle = "No Products Found!".localized
    static let addressEmptyTitle = "No Address Found In Your Address Book!".localized
    
    static let orderEmptyTitle = "No Orders Found".localized
    static let orderDescription = "There are no recent orders.".localized
    
    static let wishListTitle    = "Your wishlist is empty".localized
    static let wishDescription  = "Save items that you likes in your wishlist.".localized
    
    
}
