/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
import RATreeView
class SideMenuViewController: UIViewController,SWRevealViewControllerDelegate {
    
    var treeView : RATreeView!
    var menus : [MenuObject] = []
    let endPoint = "shopifymobile/shopifyapi/getcategorymenus?mid="
    var checker:MenuObject?=nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMenuData()
        setupTreeView()
        self.revealViewController().delegate = self
        self.revealViewController().frontViewShadowColor = .black
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let overView = UIView()
        overView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        overView.tag = 123
        overView.frame = self.revealViewController().frontViewController.view.frame
     self.revealViewController().frontViewController.view.addSubview(overView)
        self.revealViewController().frontViewController.view.bringSubviewToFront(overView)
        
    self.revealViewController().frontViewController.view.isUserInteractionEnabled = false
    self.revealViewController().view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    
   
    
    override func viewWillDisappear(_ animated: Bool) {
       super.viewWillDisappear(animated)
    self.revealViewController().frontViewController.view.isUserInteractionEnabled = true
        self.revealViewController().frontViewController.view.viewWithTag(123)?.removeFromSuperview()
    }
    
    func setupTreeView(){
        for item in view.subviews{
            if let temp=item as? RATreeView{
                temp.removeFromSuperview()
            }
        }
        treeView = RATreeView(frame: view.frame)
        treeView.register(UINib(nibName: String(describing: SideMenuTableCell.self), bundle: nil), forCellReuseIdentifier: SideMenuTableCell.className)
        treeView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        treeView.treeHeaderView = UIView()
        treeView.treeFooterView = UIView()
        view.addSubview(treeView)
        treeView.delegate = self;
        treeView.dataSource = self;
     
    }
    
    
    func getMenuData(){
        guard let url = (AppSetUp.baseUrl + endPoint + Client.merchantID).getURL() else {return}
        var request = URLRequest(url: url)
        request.httpMethod="GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = URLRequest.CachePolicy.returnCacheDataElseLoad
        self.view.addLoader()
        AF.request(request).responseData(completionHandler: {
            response in
            self.view.stopLoader()
            switch response.result {
            case .success:
                
                do {
                    if  let json = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as? [String : Any] {
                        if let jsonData=json["data"] as? [Any]{
                            print(jsonData)
                            for item in jsonData.dropFirst(0){ 
                                if let menus=item as? [String:Any]{
                                    if let submenus = menus["menus"] as? [Any]{
                                        let db = self.fechSubMenu(submenus)
                                        let temp=MenuObject(name: menus["title"] as! String, children: db, id: "123", image: "",type: "",url: "")
                                        self.menus.append(temp)
                                    }
                                    else
                                    {
                                        
                                        let temp=MenuObject(name: menus["title"] as! String, id: String(describing: menus["id"]), image: "",type: String(describing: menus["type"]),url: String(describing: menus["url"] ))
                                        
                                        self.menus.append(temp)
                                    }
                                }
                                
                            }
                        }
                        //self.menus.append(MenuObject(name: "Select Your Currency".localized, id: "", image: "",type: "currency",url: ""))
                        
                        //self.menus.append(MenuObject(name: "Language".localized, id: "", image: "",type: "lang",url: ""))
                        self.menus.append(MenuObject(name: "Privacy Policy".localized, id: "123", image: "",type: "links",url: ""))
                        self.menus.append(MenuObject(name: "Refund Policy".localized, id: "123", image: "",type: "links",url: ""))
                        self.menus.append(MenuObject(name: "Terms of services".localized, id: "123", image: "",type: "links",url: ""))
                        self.menus.append(MenuObject(name: "Contact Us".localized, id: "123", image: "",type: "contact",url: ""))
                        self.treeView.reloadData()
                        print(self.menus)
                    }
                    
                }catch let error {
                    print(error.localizedDescription)
                }
            case .failure:
                print("failure")
            }
        })
    }
    
    func fechSubMenu(_ submenus: [Any]) -> [MenuObject]{
        var dataObjects=[MenuObject]()
        for item in submenus{
            if let menus=item as? [String:Any]{
                if let submenu = menus["menus"] as? [Any]{
                    let db=fechSubMenu(submenu)
                    let temp=MenuObject(name: menus["title"] as! String, children: db, id: "123", image: "",type: "",url: "")
                    dataObjects.append(temp)
                }
                else
                {
                    
                    let db=MenuObject(name: menus["title"] as! String, id: String(describing: menus["id"]), image: "",type: String(describing: menus["type"]),url: String(describing: menus["url"] ))
                    dataObjects.append(db)
                    
                }
            }
        }
        return dataObjects
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


extension SideMenuViewController:RATreeViewDataSource {
    func treeView(_ treeView: RATreeView, numberOfChildrenOfItem item: Any?) -> Int {
        if let item = item as? MenuObject {
            return item.children.count
        } else {
            return self.menus.count
        }
    }
    
    
    func treeView(_ treeView: RATreeView, child index: Int, ofItem item: Any?) -> Any {
        
        if let item = item as? MenuObject {
            return item.children[index]
        } else {
            
            return menus[index] as AnyObject
            
        }
    }
    
    
    
    func treeView(_ treeView: RATreeView, cellForItem item: Any?) -> UITableViewCell {
        let cell = treeView.dequeueReusableCell(withIdentifier: SideMenuTableCell.className) as! SideMenuTableCell
        let item = item as! MenuObject
        
        let level = treeView.levelForCell(forItem: item)
        let detailsText = "Number of children \(item.children.count)"
        cell.selectionStyle = .none
        cell.setup(from: item, level: level)
        
        return cell
    }
    
    
}

extension SideMenuViewController:RATreeViewDelegate {
    func treeView(_ treeView: RATreeView, didSelectRowForItem item: Any) {
        let seletedMenu = item as! MenuObject
        print(seletedMenu.id)
        
        let type = returnString(strToModify:seletedMenu.type)
        
        switch type {
        /*case "currency":
            currencynavigate()
        case "lang":
            changeLanguagePressed()*/
        case "page","blog":
            if seletedMenu.children.count == 0 {
                let url = returnString(strToModify: seletedMenu.url)
                
                let viewController:WebViewController = storyboard!.instantiateViewController()
                viewController.url = ("https://" + Client.shopUrl + url).getURL()
                if let tabbarControl =  self.revealViewController().frontViewController as? TabbarController {
                    self.revealViewController().revealToggle(animated: true)
                    if let navigation = tabbarControl.viewControllers![tabbarControl.selectedIndex] as? UINavigationController {
                        navigation.pushViewController(viewController, animated: true)
                    }
                }
            }
        case "collection-all":
            if let tabbarControl =  self.revealViewController().frontViewController as? TabbarController {
                self.revealViewController().revealToggle(animated: true)
                tabbarControl.selectedIndex = 1
            }
        case "product-all":
            let viewControl:ProductListViewController = self.storyboard!.instantiateViewController()
            viewControl.isfromHome = true
            viewControl.fetchAllProduct = true
            viewControl.title = "All Products".localized
            if let tabbarControl =  self.revealViewController().frontViewController as? TabbarController {
                self.revealViewController().revealToggle(animated: true)
                if let navigation = tabbarControl.viewControllers![tabbarControl.selectedIndex] as? UINavigationController {
                    navigation.pushViewController(viewControl, animated: true)
                }
            }
        case "collection":
            if seletedMenu.children.count == 0 {
                let collectionId = returnString(strToModify: seletedMenu.id)
                
                let coll = collection(id: collectionId, title: seletedMenu.name)
                let viewControl:ProductListViewController = self.storyboard!.instantiateViewController()
                viewControl.isfromHome = true
                viewControl.collect = coll
                viewControl.title = coll.title
                if let tabbarControl =  self.revealViewController().frontViewController as? TabbarController {
                    self.revealViewController().revealToggle(animated: true)
                    if let navigation = tabbarControl.viewControllers![tabbarControl.selectedIndex] as? UINavigationController {
                        navigation.pushViewController(viewControl, animated: true)
                    }
                }
                
                
            }
        case "product":
            let viewController:ProductViewController = storyboard!.instantiateViewController()
            var productId = returnString(strToModify: seletedMenu.id)
            
            let str="gid://shopify/Product/"+productId
            let str1 = (str).data(using: String.Encoding.utf8)
            let base64 = str1!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            viewController.productId = base64
            viewController.isProductLoading = true
            if let tabbarControl =  self.revealViewController().frontViewController as? TabbarController {
                self.revealViewController().revealToggle(animated: true)
                if let navigation = tabbarControl.viewControllers![tabbarControl.selectedIndex] as? UINavigationController {
                    navigation.pushViewController(viewController, animated: true)
                }
            }
        case "links":
            Client.shared.fetchShop(completion: {
                response in
                let viewController:WebViewController = self.storyboard!.instantiateViewController()
                viewController.title = seletedMenu.name
                if seletedMenu.name == "Privacy Policy".localized{
                    viewController.url = response?.privacyPolicyUrl
                }else if seletedMenu.name == "Refund Policy".localized{
                    viewController.url = response?.refundPolicyUrl
                }else if seletedMenu.name == "Terms of services".localized{
                    viewController.url = response?.termsOfService
                }else{}
                if let tabbarControl =  self.revealViewController().frontViewController as? TabbarController {
                    self.revealViewController().revealToggle(animated: true)
                    if let navigation = tabbarControl.viewControllers![tabbarControl.selectedIndex] as? UINavigationController {
                        navigation.pushViewController(viewController, animated: true)
                    }
                }
            })
        case "contact":
            let contactus:ContactUsViewController = self.storyboard!.instantiateViewController()
            contactus.title = "Contact Us".localized
            if let tabbarControl =  self.revealViewController().frontViewController as? TabbarController {
                self.revealViewController().revealToggle(animated: true)
                if let navigation = tabbarControl.viewControllers![tabbarControl.selectedIndex] as? UINavigationController {
                    navigation.pushViewController(contactus, animated: true)
                }
            }
        default:
            if seletedMenu.children.count == 0 {
                let url = returnString(strToModify: seletedMenu.url)
                let viewController:WebViewController = storyboard!.instantiateViewController()
                if url.contains("https://"){
                    viewController.url = url.getURL()
                }else{
                    viewController.url = ("https://" + Client.shopUrl + url).getURL()
                }
                if let tabbarControl =  self.revealViewController().frontViewController as? TabbarController {
                    self.revealViewController().revealToggle(animated: true)
                    if let navigation = tabbarControl.viewControllers![tabbarControl.selectedIndex] as? UINavigationController {
                        navigation.pushViewController(viewController, animated: true)
                    }
                }
            }
        }
        
    }
    // use this when switching the language
    /*func changeLanguagePressed()
    {
        let Stores = ["English","Arabic"]
        if value[0]=="ar"
        {
            self.revealViewController().rightRevealToggle(animated: true)
        }else{
            self.revealViewController().revealToggle(animated: true)
        }
        let actionsheet = UIAlertController(title: "Select Language".localized, message: nil, preferredStyle: .alert)
        
        for item in Stores {
            actionsheet.addAction(UIAlertAction(title: item, style: UIAlertActionStyle.default,handler: {
                action -> Void in
                self.selectStore(store:item)
            }))
            
        }
        actionsheet.addAction(UIAlertAction(title: "Cancel".localized, style: UIAlertActionStyle.cancel, handler: {
            action -> Void in
        }))
        
        self.present(actionsheet, animated: true, completion: nil)
    }
    func selectStore(store:String){
        if store == "English"
        {
            UserDefaults.standard.removeObject(forKey: "AppleLanguages")
            UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
        }
        if store == "Arabic"
        {
            UserDefaults.standard.removeObject(forKey: "AppleLanguages")
            UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
        }
        load_app()
    }*/
    func currencynavigate(){
        self.revealViewController().revealToggle(animated: true)
        let currency = ["AED","BHD","SAR","EGP","KWD","OMR","EUR","USD","GBP"]
        let actionsheet = UIAlertController(title: "Select Currency", message: nil, preferredStyle: .alert)
        for item in currency {
            let action = UIAlertAction(title: item, style: UIAlertAction.Style.default,handler: {
                action -> Void in
                self.selectCurrencyStore(store:item)
            })
            let image = UIImage(named: item)
            action.setValue(image?.withRenderingMode(.alwaysOriginal), forKey: "image")
            actionsheet.addAction(action)
        }
        actionsheet.addAction(UIAlertAction(title: "Cancel".localized, style: UIAlertAction.Style.cancel, handler: {
            action -> Void in
        }))
        self.present(actionsheet, animated: true, completion: nil)
    }
    func selectCurrencyStore(store:String){
        UserDefaults.standard.removeObject(forKey: "currency")
        UserDefaults.standard.set(store, forKey: "currency")
        load_app()
    }
    func load_app()
    {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SWRevealViewController")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { (finished) -> Void in
        }
    }
    
    func returnString(strToModify:String)->String{
        if strToModify.contains("Optional"){
            let sel=strToModify.components(separatedBy: "(")
            let selected=sel[1].components(separatedBy: ")")
            return selected[0]
        }
        return strToModify
    }
}
