
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */



import UIKit

class EditUserViewController: UIViewController {

    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var passwordHeight: NSLayoutConstraint!
    @IBOutlet weak var checkBoxButton: UIButton!
    var userDetails=[String:String]()
    @IBOutlet weak var cnfPasswordHeight: NSLayoutConstraint!
   
    @IBOutlet weak var cnfpassword: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var lastNam: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var viewHieght: NSLayoutConstraint!
    var flag=false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addLoader()
        Client.shared.fetchCustomerDetails {
            response,error   in
            self.view.stopLoader()
            if let response = response {
                self.firstName.text=response.firstName
                self.lastNam.text=response.lastName
                self.email.text=response.email
            }else {
                //self.showErrorAlert(error: error?.localizedDescription)
            }
        }
            
        setupPage()
            
        checkBoxButton.addTarget(self, action: #selector(checkBoxClicked), for: .touchUpInside)
    }
    
    
    func setupPage(){
        checkBoxButton.tag=0
        checkBoxButton.setImage(UIImage(named: "unchecked"), for: .normal)
        password.isHidden=true
        cnfpassword.isHidden=true
        passwordHeight.constant=0
     
        cnfPasswordHeight.constant=0
        viewHieght.constant-=120
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkBoxClicked(){
        if checkBoxButton.tag==0
        {
            checkBoxButton.tag=10
            checkBoxButton.setImage(UIImage(named: "checked"), for: .normal)
            flag=true
            password.isHidden=false
            cnfpassword.isHidden=false
            passwordHeight.constant=40
            cnfPasswordHeight.constant=40
            viewHieght.constant+=120
        }
        else
        {
            checkBoxButton.tag=0
            password.isHidden=true
            cnfpassword.isHidden=true
            checkBoxButton.setImage(UIImage(named: "unchecked"), for: .normal)
            flag=false
            passwordHeight.constant=0
            cnfPasswordHeight.constant=0
            viewHieght.constant-=120
        }
    }
    @IBAction func saveButtonTapped(_ sender: Any) {
  
        if firstName.text=="" || lastNam.text=="" || email.text==""{
            self.showErrorAlert(error: "All fields are required.".localized)
            return
        }
        if !(firstName.text?.isValidName())!
        {
       
             self.showErrorAlert(error: "Invalid First Name.".localized)
            return
        }
        if !(lastNam.text?.isValidName())!
        {
        
            self.showErrorAlert(error: "Invalid Last Name.".localized)
            return
        }
        if !(email.text?.isValidEmail())!
        {
        
            self.showErrorAlert(error: "Invalid Email ID.".localized)
            return
        }
      
        if flag==true
        {
            
            if password.text == "" || cnfpassword.text == ""
            {
             
                 self.showErrorAlert(error: "All Fields are Required.".localized)
                return
            }
            if password.text != cnfpassword.text
            {
              
                self.showErrorAlert(error: "Please enter same password for confirm password.".localized)
                return
            }
            
        }
        self.view.addLoader()
        Client.shared.updateCustomer(with: firstName.text!, lname: lastNam.text!, email: email.text!, password:password.text! ){
            response,error,netError  in
            self.view.stopLoader()
            if let response = response{
                self.showErrorAlert(title: "Success".localized, error: "Customer Data Updated.".localized)
            }else if let error = error {
               self.showErrorAlert(errors: error)
            }else {
                self.showErrorAlert(error: netError?.localizedDescription)
            }
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }


}
