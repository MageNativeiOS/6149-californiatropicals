/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class ProductImageViewController: UIViewController , UIScrollViewDelegate{

    @IBOutlet weak var dissmissView: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var img: UIImageView!
    var images : [ImageViewModel]?
    var imageurl = ""
    override func viewDidLoad() {
        super.viewDidLoad()
            collectionView.isHidden = true
            scrollview.isHidden = false
            scrollview.maximumZoomScale = 6.0
            scrollview.minimumZoomScale = 0.25
            scrollview.delegate = self
            
            self.img.sd_setImage(with: imageurl.getURL(), placeholderImage: UIImage(named: "placeholder"),completed: {
                a,b,c,d in
                SDWebImageDownloader.shared.downloadImage(with: self.imageurl.getURL(), options: [.useNSURLCache,.highPriority], progress: nil, completed: {
                              image,_,_,_ in
                               self.img.image = image
                          })
            })
        
        
        dissmissView.addTarget(self, action: #selector(self.dismissView(_:)), for: .touchUpInside)
        self.view.bringSubviewToFront(dissmissView)
        
        
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView){
        let imgViewSize:CGSize! = self.img.frame.size;
        let imageSize:CGSize! = self.img.image?.size;
        var realImgSize : CGSize;
        if(imageSize.width / imageSize.height > imgViewSize.width / imgViewSize.height) {
            realImgSize = CGSize(width: imgViewSize.width,height: imgViewSize.width / imageSize.width * imageSize.height);
        }
        else {
            realImgSize = CGSize(width: imgViewSize.height / imageSize.height * imageSize.width, height: imgViewSize.height);
        }
        var fr:CGRect = CGRect.zero
        fr.size = realImgSize;
        self.img.frame = fr;

        let scrSize:CGSize = scrollView.frame.size;
        let offx:CGFloat = (scrSize.width > realImgSize.width ? (scrSize.width - realImgSize.width) / 2 : 0);
        let offy:CGFloat = (scrSize.height > realImgSize.height ? (scrSize.height - realImgSize.height) / 2 : 0);
        scrollView.contentInset = UIEdgeInsets(top: offy, left: offx, bottom: offy, right: offx);

        // The scroll view has zoomed, so you need to re-center the contents
        let scrollViewSize:CGSize = self.scrollViewVisibleSize();

        // First assume that image center coincides with the contents box center.
        // This is correct when the image is bigger than scrollView due to zoom
        var imageCenter:CGPoint = CGPoint(x: self.scrollview.contentSize.width/2.0, y:
                                          self.scrollview.contentSize.height/2.0);

        let scrollViewCenter:CGPoint = self.scrollViewCenter()

        //if image is smaller than the scrollView visible size - fix the image center accordingly
        if (self.scrollview.contentSize.width < scrollViewSize.width) {
            imageCenter.x = scrollViewCenter.x;
        }

        if (self.scrollview.contentSize.height < scrollViewSize.height) {
            imageCenter.y = scrollViewCenter.y;
        }

        self.img.center = imageCenter;

    }
    func scrollViewCenter() -> CGPoint {
        let scrollViewSize:CGSize = self.scrollViewVisibleSize()
        return CGPoint(x: scrollViewSize.width/2.0, y: scrollViewSize.height/2.0);
    }
    // Return scrollview size without the area overlapping with tab and nav bar.
    func scrollViewVisibleSize() -> CGSize{

        let contentInset:UIEdgeInsets = self.scrollview.contentInset;
        let scrollViewSize:CGSize = self.scrollview.bounds.standardized.size;
        let width:CGFloat = scrollViewSize.width - contentInset.left - contentInset.right;
        let height:CGFloat = scrollViewSize.height - contentInset.top - contentInset.bottom;
        return CGSize(width:width, height:height);
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.img
    }
    
    
    @objc func dismissView(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ProductImageViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell       = collectionView.dequeueReusableCell(withReuseIdentifier: "productImage", for: indexPath)
        if let contentView =  cell.viewWithTag(852) as? UIImageView {
            contentView.setImageFrom(images?[indexPath.row].url)
        }
        return cell
    }
    
}

extension ProductImageViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}
