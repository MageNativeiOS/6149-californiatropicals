//
//  HomeCollectionCell.swift
//  MageNative Shopify App
//
//  Created by Manohar Singh Rawat on 28/01/20.
//  Copyright © 2020 MageNative. All rights reserved.
//

import UIKit

protocol productClicked:class {
    func productCellClicked(product:ProductViewModel,sender:Any)
}

class HomeCollectionCell: UITableViewCell {

    @IBOutlet weak var collectionImageView: UIImageView!
    
    @IBOutlet weak var topLabel: UILabel!
    
    fileprivate var products: PageableArray<ProductListViewModel>!
    
    @IBOutlet weak var productsCollectionView: UICollectionView!
    
    
    @IBOutlet weak var viewAll: UIButton!
    
    var delegate: productClicked?
    var scrollHorizontal = false;
    var parentView = HomeViewController()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.productsCollectionView.delegate = self;
        self.productsCollectionView.dataSource = self;
        // Initialization code
    }

    func configure(from model: collection?){
        guard let model = model else{return;}
        self.topLabel.text = model.title;
        //self.topLabel.text = String(htmlEncodedString: model.title)
        Client.shared.fetchProducts(coll: model, sortKey: Storefront.ProductCollectionSortKeys.bestSelling, limit: 9) { (response, image, error) in
            self.products = response;
            self.collectionImageView.setImageFrom(image)
            self.productsCollectionView.reloadData();
        }
        
        if !scrollHorizontal{
            //productsCollectionView.isScrollEnabled = true;
            if let layout = productsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout{
                layout.scrollDirection = .vertical
            }
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension HomeCollectionCell: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products?.items.count ?? 0;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.className, for: indexPath) as! ProductCollectionViewCell
        cell.delegate = self
        cell.setupView((products?.items[indexPath.row].model?.node.viewModel)!)
        return cell;
        
    }
    
}
extension HomeCollectionCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if scrollHorizontal {
           return collectionView.calculateCellSize(numberOfColumns: 2)
        }
        return CGSize(width: collectionView.frame.width/3-1, height: 100)
    }
}
extension HomeCollectionCell:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.productCellClicked(product: (products.items[indexPath.row].model?.node.viewModel)!, sender: self)
    }
}
extension HomeCollectionCell:wishListDelegate{
    func addToWishListProduct(_ cell: ProductCollectionViewCell, didAddToWishList sender: Any) {
        guard let indexPath = self.productsCollectionView.indexPath(for: cell) else {return}
        let product = self.products.items[indexPath.row]
        guard  let viewModel = product.model?.node.viewModel else {return}
        let wishProduct = CartProduct.init(product: viewModel, variant: product.variants.items.first!)
        if WishlistManager.shared.isProductinWishlist(product: viewModel) {
            WishlistManager.shared.removeFromWishList(wishProduct)
        }else {
            WishlistManager.shared.addToWishList(wishProduct)
        }
        self.productsCollectionView.reloadItems(at: [indexPath])
        parentView.setupTabbarCount()
    }
}
