//
//  HomeRecentlyViewedCell.swift
//  MageNative Shopify App
//
//  Created by Manohar Singh Rawat on 28/01/20.
//  Copyright © 2020 MageNative. All rights reserved.
//

import UIKit

class HomeRecentlyViewedCell: UITableViewCell {

    @IBOutlet weak var productsCollectionView: UICollectionView!
    @IBOutlet weak var topLabel: UILabel!
    
    var parentView = HomeViewController()
       var products: [ProductViewModel]!{
           didSet {
               self.productsCollectionView.reloadData()
           }
       }
         var delegate:productClicked?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        productsCollectionView.delegate = self;
        productsCollectionView.dataSource = self;
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension HomeRecentlyViewedCell:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell    = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.className, for: indexPath) as! ProductCollectionViewCell
        let product = self.products[indexPath.item]
        cell.setupView(product)
        cell.delegate = self
        return cell
    }
}

extension HomeRecentlyViewedCell:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.productCellClicked(product: products[indexPath.row], sender: self)
    }
}

extension HomeRecentlyViewedCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if  UIDevice.current.model.lowercased() == "ipad".lowercased() {
            return collectionView.calculateCellSize(numberOfColumns: 6)
        }else {
            return CGSize(width: collectionView.frame.width/2, height: collectionView.frame.height - 10)
            
        }
    }
}


extension HomeRecentlyViewedCell:wishListDelegate{
    func addToWishListProduct(_ cell: ProductCollectionViewCell, didAddToWishList sender: Any) {
        guard let indexPath = self.productsCollectionView.indexPath(for: cell) else {return}
        let product = self.products[indexPath.row]
        let wishProduct = CartProduct.init(product: product, variant: product.variants.items.first!)
        if WishlistManager.shared.isProductinWishlist(product: product) {
            WishlistManager.shared.removeFromWishList(wishProduct)
        }else {
            WishlistManager.shared.addToWishList(wishProduct)
        }
        self.productsCollectionView.reloadItems(at: [indexPath])
    }
}
