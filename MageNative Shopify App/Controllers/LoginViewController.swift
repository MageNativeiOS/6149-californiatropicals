/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
import SDWebImage

class LoginViewController: UIViewController {
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var login: Button!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var dismissView: UIBarButtonItem!
    
    @IBOutlet weak var loginbg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let img = UserDefaults.standard.value(forKey: "loginbg") as! String
        self.loginbg.sd_setImage(with: URL(string: img))
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        dismissView.target = self
        dismissView.action = #selector(self.dismissViewControl)
        login.addTarget(self, action: #selector(self.doLogin), for: .touchUpInside)
    }
    
    
    @objc func dismissViewControl(){
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @objc func doLogin(){
        guard let usernameText = username.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else {return}
        guard let password = password.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else {return}
        if usernameText == "" || password ==  ""  {
            self.showErrorAlert(error: "Username or password is empty.".localized)
           return
        }
        
        self.view.addLoader()
        Client.shared.customerAccessToken(email: usernameText, password: password, completion: {
            token,usererror,error in
            self.view.stopLoader()
            UserDefaults.standard.set(password, forKey: "password")
             guard let token = token else {
                if let usererror = usererror {
                    if usererror.first?.errorMessage.lowercased() == "Unidentified Customer".lowercased() {
                             self.showErrorAlert(error: "Username or Password is Incorrect.".localized)
                    }else {
                    //self.showErrorAlert(errors: usererror)
                    }
                    return
                }else {
                    //self.showErrorAlert(error: error?.localizedDescription)
                    
                }
                return
                
            }
            Client.shared.saveCustomerToken(token: token.accessToken, expiry: token.expireAt, email: usernameText, password: password)
             self.dismiss(animated: true, completion: {
                self.presentingViewController?.tabBarController?.selectedIndex = 0
            })
        })
    }
}
