/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class AccountViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var shopDetails:ShopViewModel?
    var accountOptions = [["Profile".localized,"Address Book".localized,"My Orders".localized],/*["Privacy Policy".localized,"Return Policy".localized,"Terms of services".localized,"Contact Us".localized],*/["Sign out".localized]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Client.shared.fetchShop(completion: {
            response in
            self.shopDetails = response
            self.tableView.reloadData()
        })
        setupTable()
        setUPTableHeader()
    }
    
    func setupTable(){
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    func setUPTableHeader(){
        let cell = tableView.dequeueReusableCell(withIdentifier: AccountProfileCell.className) as! AccountProfileCell
        self.view.addLoader()
        Client.shared.fetchCustomerDetails(completeion: {
            response,error   in
            if let response = response {
                self.view.stopLoader()
                cell.displayName.text = "Hey ".localized + (response.displayName?.capitalized)!
                if response.firstName == nil || response.firstName == ""{
                    cell.nameInitials.text = (response.displayName?.first?.description)!.uppercased()
                }else{
                    var name = ""
                    name += (response.firstName?.first?.description)!
                    name +=  (response.lastName?.first?.description)!
                    cell.nameInitials.text =  name.uppercased()
                }
                self.tableView.tableHeaderView = cell
            }else {
                //self.showErrorAlert(error: error?.localizedDescription)
            }
        })
        //cell.backgroundColor = UIColor.lightGray
        self.tableView.tableHeaderView = cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
}
extension AccountViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "My Account".localized
        default:
            return nil
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            let title = self.accountOptions[indexPath.section][indexPath.row]
            
            if title == "My Orders".localized{
                let orderViewControl:OrdersViewController = self.storyboard!.instantiateViewController()
                self.navigationController?.pushViewController(orderViewControl, animated: true)
            }else if title ==  "Address Book".localized {
                let addressViewControl:AddressesViewController = self.storyboard!.instantiateViewController()
                self.navigationController?.pushViewController(addressViewControl, animated: true)
            }else if title ==  "Profile".localized {
                let editProfileView:EditUserViewController = self.storyboard!.instantiateViewController()
                self.navigationController?.pushViewController(editProfileView, animated: true)
            }
        /*case 1:
            let name = self.accountOptions[indexPath.section][indexPath.row]
            self.gotoWebPages(page: name)*/
        case 1:
            self.doLogOut()
        default:
            print("Hello")
        }
    }
}

extension AccountViewController:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountOptions[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return accountOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AccountTableViewCell.className) as! AccountTableViewCell
        cell.title.text = accountOptions[indexPath.section][indexPath.row]
        return cell
    }
}

extension AccountViewController{
    func gotoWebPages(page:String){
        let viewController:WebViewController = storyboard!.instantiateViewController()
        viewController.title = page
        switch page {
        case "Privacy Policy".localized:
            viewController.url = shopDetails?.privacyPolicyUrl
            
        case "Return Policy".localized:
            print("sdsd")
            viewController.url = shopDetails?.refundPolicyUrl
        case "Terms of services".localized:
            viewController.url = shopDetails?.termsOfService
            print("sdsd")
        default:
            let contactus:ContactUsViewController = self.storyboard!.instantiateViewController()
            contactus.title = "Contact Us".localized
            self.navigationController?.pushViewController(contactus, animated: true)
            return
            
        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func doLogOut(){
        if Client.shared.doLogOut() {
            self.tabBarController?.selectedIndex = 0
        }
    }
}

