/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class WishlistViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    static let shared = WishlistViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUPTable()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
            self.tableView.reloadData()
            self.setupTabbarCount()
    }
    
    func setUPTable(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.tableFooterView = UIView()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
}


extension WishlistViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            
             WishlistManager.shared.removeFromWishList(WishlistManager.shared.wishListProducts[indexPath.row])
            tableView.reloadData()
            self.setupTabbarCount()
        default:
            break
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let product         = WishlistManager.shared.wishListProducts[indexPath.row]
        let productViewController:ProductViewController = self.storyboard!.instantiateViewController()
        productViewController.product = product.productModel
        self.navigationController?.pushViewController(productViewController, animated: true)
    }
    
    
    
    
}


extension WishlistViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return WishlistManager.shared.wishListProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WishListCell.className) as! WishListCell
        
        cell.configure(from: WishlistManager.shared.wishListProducts[indexPath.row].viewModel)
        return cell
    }
}


extension WishlistViewController:DZNEmptyDataSetSource,DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: EmptyData.wishListTitle)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
         return NSAttributedString(string: EmptyData.wishDescription)
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return WishlistManager.shared.wishListProducts.count == 0
    }
    
    
}
