/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class AddAddressViewController: UIViewController {
    
    @IBOutlet weak var fname: UITextField!
    @IBOutlet weak var lname: UITextField!
    @IBOutlet weak var address1: UITextField!
    @IBOutlet weak var address2: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var zip: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var saveAddress: UIButton!
    
    @IBOutlet weak var selectCountry: UIButton!
    @IBOutlet weak var selectState: UIButton!
    
    var edirAddress:AddressesViewModel!
    var isEditAddress = false
    override func viewDidLoad() {
        super.viewDidLoad()
        saveAddress.addTarget(self, action: #selector(self.saveAddress(_:)), for: .touchUpInside)
        selectCountry.addTarget(self, action: #selector(self.showCountry(_:)), for: .touchUpInside)
        
        if isEditAddress == true{
            fname.text = edirAddress.firstName!
            lname.text = edirAddress.lastName
            address1.text = edirAddress.address1
            address2.text = edirAddress.address2
            city.text = edirAddress.city
            selectCountry.setTitle(edirAddress.country, for: .normal)
            print(edirAddress.province)
            Country.shared.stateWithCountry(with: edirAddress.country!)
            if edirAddress.province == "" || edirAddress.province == nil{
                selectState.isHidden = true
            }else{
                self.selectState.isHidden = false
                selectState.setTitle(edirAddress.province, for: .normal)
            }
            zip.text = edirAddress.zip
            phone.text = edirAddress.phone
        }
        addBorders(selectCountry)
        addBorders(selectState)
    }
    
    func addBorders(_ sender: UIButton)
    {
        sender.layer.borderWidth = 0.5
        sender.layer.borderColor = UIColor.lightGray.cgColor
    }
   
    @objc func showCountry(_ sender:UIButton){
     
        let dropDown = DropDown(anchorView: sender)
        dropDown.dataSource = Country.shared.countries!
        dropDown.selectionAction = {[unowned self](index, item) in
            sender.setTitle(item, for: .normal);
            Country.shared.stateWithCountry(with: item)
            guard let states = Country.shared.currentStates else {return}
            if states.count == 0{
                self.selectState.isHidden = true
            }else{
                self.selectState.addTarget(self, action: #selector(self.showStates(_:)), for: .touchUpInside)
                self.selectState.isHidden = false
                self.selectState.setTitle("Select State".localized, for: .normal)
            }
        }
        
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        if dropDown.isHidden {
            let _ = dropDown.show();
        } else {
            dropDown.hide();
        }
    }
    
    @objc func showStates(_ sender:UIButton){
        guard let states = Country.shared.currentStates else {
            
            return }
        let dropDown = DropDown(anchorView: sender)
        dropDown.dataSource = states as! [String]
        dropDown.selectionAction = {(index, item) in
            sender.setTitle(item, for: UIControl.State());
            
        }
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        if dropDown.isHidden {
            let _ = dropDown.show();
        } else {
            dropDown.hide();
        }
    }
    
    
    
    @objc func saveAddress(_ sender:UIButton){
        guard let fname         = fname.text else {return}
        guard let lname         = lname.text else {return}
        guard let address1      = address1.text else {return}
        guard let address2      = address2.text else {return}
        guard let city          = city.text else {return}
        guard let zip           = zip.text else {return}
        guard let country       = selectCountry.titleLabel?.text  else {return}
        guard let state         = selectState.titleLabel?.text else {return}
        guard let phone         = phone.text else {return}
       if fname == "" || lname == "" || address1 == "" || address2 == "" || city == "" || zip  == "" || country == "Select Country".localized || phone == "" {
            self.showErrorAlert(error: "All fields are required!.".localized)
            return
        }
        if Country.shared.currentStates?.count != 0 && state == "Select State".localized{
            self.showErrorAlert(error: "All fields are required!.".localized)
            return
        }
        var addressFields = [String:String]()
        if Country.shared.currentStates?.count == 0{
            addressFields = ["firstName":fname,"lastName":lname,"address1":address1,"address2":address2,"city":city,"zip":zip,"country":country,"province":"","phone":phone]
        }else{
            addressFields = ["firstName":fname,"lastName":lname,"address1":address1,"address2":address2,"city":city,"zip":zip,"country":country,"province":state,"phone":phone]
        }
        
        
        print(addressFields)
        if isEditAddress == true{
            do {
                self.view.addLoader()
                Client.shared.customerUpdateAddress(address: addressFields, addressId: edirAddress.id!, completeion: {[unowned self]
                    response ,error in
                    self.view.stopLoader()
                    if let response = response {
                        self.navigationController?.popViewController(animated: true)
                        return
                    }else{
                        self.showErrorAlert(errors: error)
                    }
                })
            }catch let error {
                print(error.localizedDescription)
            }
        }else{
            do {
             
                self.view.addLoader()
                Client.shared.customerAddAddress(address: addressFields, completeion: {[unowned self]
                    response ,error in
                    self.view.stopLoader()
                    if let response = response {
                       if response.name != nil {
                           self.navigationController?.popViewController(animated: true)
                            return
                        }
                    }else {
                        //self.showErrorAlert(errors: error)
                    }
                })
            }catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    
}

