/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class LaunchScreenControl: UIViewController {

    @IBOutlet weak var statusImage: UIImageView!
    let appStatusEndPoint = "index.php/shopifymobile/shopifyapi/getstatus?mid="+Client.merchantID+"&device_type=ios"
    var appStatusViewModel = LaunchScreenViewModel.self
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getAppStatus()
    }
    override var prefersStatusBarHidden:Bool{
        return true
    }
    
    func getAppStatus(){
        guard let url = (AppSetUp.baseUrl + appStatusEndPoint).getURL() else {return}
        let request = URLRequest(url: url)
         AF.request(request).responseJSON(completionHandler: {
            response in
            switch response.result{
            case .success:
                do {
                    response.value.map({
                        let json = $0 as! [String:Any]
                        guard let activeString = json["status"] as? String else {return}
                        if activeString == "active" {
                            self.navigateToHome()
                        }else if activeString == "inactive" {
                            self.showErrorAlert(error: "App is inactive.".localized)
                        }else if activeString == "expired" {
                            self.statusImage.image = #imageLiteral(resourceName: "trial_image")
                        }else if activeString == "not-present"{
                              self.showErrorAlert(error: "App is available.Please install MageNative App on your store.".localized)
                        }
                    })
                }
            case .failure(let error):
                self.showErrorAlert(error: error.localizedDescription)
                print(error.localizedDescription)
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
      
    }
    
    //Mark: Navigate to home
    func navigateToHome(){
        Client.shared.fetchShop(completion: {
            shopDetails in
            if let currenCode = shopDetails?.currencyCode {
                Client.shared.saveCurrencyCode(currency: currenCode)
            }
            let mainTabbar:SWRevealViewController = self.storyboard!.instantiateViewController()
            mainTabbar.modalPresentationStyle = .fullScreen
            self.present(mainTabbar, animated: false, completion: nil)
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    

   

}
